import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewyorktimesService {

  urlNewsNYT = 'https://newsapi.org/v2/everything?domains=wsj.com&apiKey=f0e3efa1508c4b6c9645d91e28472937';
  constructor(private http: HttpClient) { }

  getAllNewsNYT(){
    return this.http.get(this.urlNewsNYT);
  }
}
