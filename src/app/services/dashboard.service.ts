import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  apiNewsItBusiness = 'https://newsapi.org/v2/top-headlines?country=it&category=business&apiKey=f0e3efa1508c4b6c9645d91e28472937';

  constructor(private http: HttpClient) { }

  getAllNews(){
    return this.http.get(this.apiNewsItBusiness);
  }

  getNewsStateCategory(stato: string, categoria: string){
       return this.http.get('https://newsapi.org/v2/top-headlines?country='+stato+'&category='+categoria+'&apiKey=f0e3efa1508c4b6c9645d91e28472937')
  }
}
