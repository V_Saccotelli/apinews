import { RoutguardService } from './../services/routeguard.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email!: any;
password!: any;
  constructor(private guardservice: RoutguardService,
    private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    this.guardservice.loginApp(this.email, this.password);
    this.router.navigate(['news']);
  }

}
