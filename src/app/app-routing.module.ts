import { RoutguardService } from './services/routeguard.service';
import { NewyorktimesComponent } from './newyorktimes/newyorktimes.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'news', component: DashboardComponent},
  {path: 'newyorktimes', component: NewyorktimesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
