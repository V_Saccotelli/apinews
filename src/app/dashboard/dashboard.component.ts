import { DashboardService } from './../services/dashboard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  newsItaBusiness!: any;
  stato!: any;
  categoria!: any;
  constructor(private dashService: DashboardService) { }

  ngOnInit(): void {
    this.dashService.getAllNews()
    .subscribe(resp =>  {
      console.log(resp)
      this.newsItaBusiness = resp
    });
  }

  cercaNews(){
  
     this.dashService.getNewsStateCategory(this.stato, this.categoria)
     .subscribe(resp => this.newsItaBusiness = resp);

     this.stato= '';
     this.categoria= '';
  }

}
