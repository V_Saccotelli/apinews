import { NewyorktimesService } from './../services/newyorktimes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newyorktimes',
  templateUrl: './newyorktimes.component.html',
  styleUrls: ['./newyorktimes.component.css']
})
export class NewyorktimesComponent implements OnInit {

  constructor(private nytService: NewyorktimesService) { }

  nytNews!: any;
  ngOnInit(): void {
    this.nytService.getAllNewsNYT()
    .subscribe(resp => {
      console.log(resp);
      this.nytNews = resp
    });
  }

}
