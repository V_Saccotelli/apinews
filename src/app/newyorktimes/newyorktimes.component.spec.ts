import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewyorktimesComponent } from './newyorktimes.component';

describe('NewyorktimesComponent', () => {
  let component: NewyorktimesComponent;
  let fixture: ComponentFixture<NewyorktimesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewyorktimesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewyorktimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
